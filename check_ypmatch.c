/* Copyright (C) 2011 Per Cederqvist <ceder@lysator.liu.se>

   This program is free software; you can redistribute it and/or modify
   it under the terms of the GNU General Public License version 2 as
   published by the Free Software Foundation.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program; if not, write to the Free Software Foundation,
   Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.  */


#define PROGRAM "check_ypmatch"
#define VERSION "1.0"

#include <errno.h>
#include <netdb.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <string.h>
#include <signal.h>
#include <rpc/rpc.h>
#include <rpcsvc/yp.h>

static int verbosity = 0;

/* Name and version of program.  */
/* Print the version information.  */
static void
print_version(void)
{
  printf("%s %s\n", PROGRAM, VERSION);
  printf("Copyright (C) 2011 Per Cederqvist.\n");
}

static void
print_usage (FILE *fp)
{
  fprintf(fp, "Usage: %s [-d domain] [-h hostname] [-m map] [-u user]\n",
	  PROGRAM);
  fprintf(fp, "map defaults to passwd.byname, and user to nobody.\n");
}

static void
bad_usage(const char *msg)
{
  fprintf(stderr, PROGRAM ": bad usage: %s.\n", msg);
  exit(3);
}

static double
timeval_diff_d(struct timeval a, struct timeval b)
{
    return (a.tv_sec - b.tv_sec) + 1e-6 * (a.tv_usec - b.tv_usec);
}

void
resolve_host(const char *hostname, struct sockaddr_in *hostaddr)
{
  memset(hostaddr, 0, sizeof(*hostaddr));
  hostaddr->sin_family = AF_INET;

  if (inet_aton (hostname, &hostaddr->sin_addr) == 0)
    {
      struct hostent *host = gethostbyname(hostname);
      if (!host)
	{
	  fprintf(stderr, "%s: cannot resolve host %s\n",
		  PROGRAM, hostname);
	  exit(3);
	}
      memcpy(&hostaddr->sin_addr, host->h_addr_list[0],
	     sizeof(hostaddr->sin_addr));
    }
}

static const char *timeout_reason;

static void
timeout_handler(int sig)
{
  printf("CRITICAL - %s\n", timeout_reason);
  exit(2);
}

static int
perform_match(const char *hostname, const char *domain,
	      const char *map, const char *key,
	      double total_timeout, double critical, double warning)
{
  struct sockaddr_in hostaddr;
  struct timeval retransmitt;
  int sock;
  CLIENT *client;
  int res = 0;
  struct sigaction timeout_action;

  memset(&timeout_action, 0, sizeof(timeout_action));
  timeout_action.sa_handler = timeout_handler;
  sigemptyset(&timeout_action.sa_mask);
  timeout_action.sa_flags = 0;
  timeout_reason = "portmap timeout";

  if (sigaction(SIGALRM, &timeout_action, NULL) < 0)
    {
      printf("UNKNOWN - sigaction failed: %s\n", strerror(errno));
      return 3;
    }

  alarm(total_timeout);

  ypreq_key req;
  ypresp_val resp;
  struct timeval timeout;
  enum clnt_stat result;

  struct timeval t0;
  struct timeval t1;
  struct timeval t2;

  resolve_host(hostname, &hostaddr);
  retransmitt.tv_sec = 1;
  retransmitt.tv_usec = 0;
  sock = RPC_ANYSOCK;

  req.domain = (char*)domain;
  req.map = (char*)map;
  req.key.keydat_val = (char*)key;
  req.key.keydat_len = strlen(key);
  memset(&resp, 0, sizeof(resp));

  gettimeofday(&t0, NULL);
  client = clntudp_create(&hostaddr, YPPROG, YPVERS, retransmitt, &sock);
  if (client == NULL)
    {
      printf("CRITICAL - %s", clnt_spcreateerror("clntudp_create failed"));
      return 2;
    }
  timeout_reason = "ypserv timeout";
  gettimeofday(&t1, NULL);
  double d_portmap = timeval_diff_d(t1, t0);
  total_timeout -= d_portmap;
  timeout.tv_sec = (int)total_timeout;
  timeout.tv_usec = (total_timeout - timeout.tv_sec) * 1.0e6;
  result = clnt_call(client, YPPROC_MATCH, (xdrproc_t) xdr_ypreq_key,
		     (caddr_t) &req, (xdrproc_t) xdr_ypresp_val,
		     (caddr_t) &resp, timeout);
  alarm(0);
  if (result != RPC_SUCCESS)
    {
      printf("CRITICAL - clnt_call failed: %s", clnt_sperrno(result));
      return 2;
    }
  gettimeofday(&t2, NULL);

  double d_match = timeval_diff_d(t2, t1);
  double d_tot = timeval_diff_d(t2, t0);

  if (critical > 0 && d_tot >= critical)
    res = 2;
  else if (warning > 0 && d_tot >= warning)
    res = 1;

  if (resp.val.valdat_len == 0)
    res = 2;

  switch(res)
    {
    case 0:
      printf("OK - ");
      break;
    case 1:
      printf("WARNING - ");
      break;
    case 2:
      printf("CRITICAL - ");
      break;
    }
  if (resp.val.valdat_len == 0)
    printf("%s not found in %s. ", key, map);
  else
    printf("%s found. ", key);
  printf("Response time %f s. | ", d_tot);

  printf(" portmap=%fs;;;;", d_portmap);
  printf(" match=%fs;;;;", d_match);
  printf(" total=%fs;", d_tot);
  if (warning > 0)
    printf("%f;", warning);
  else
    printf(";");
  if (critical > 0)
    printf("%f;", critical);
  else
    printf(";");
  printf(";\n");
  return res;
}

int
main (int argc, char **argv)
{
  char *nis_dom = NULL;
  char *hostname = NULL;
  char *map = "passwd.byname";
  char *key = "nobody";
  double timeout = 10;
  double warning = -1;
  double critical = -1;

  while (1)
    {
      int c = getopt(argc, argv, "H:t:w:c:vBhd:m:k:");
      if (c == (-1))
        break;
      switch (c)
        {
	case 'H':
	  hostname = optarg;
	  break;
	case 't':
	  timeout = strtod(optarg, NULL);
	  break;
	case 'w':
	  warning = strtod(optarg, NULL);
	  break;
	case 'c':
	  critical = strtod(optarg, NULL);
	  break;
	case 'v':
	  ++verbosity;
	  break;
	case 'V':
	  print_version();
	  return 0;
	case 'h':
	  print_usage(stdout);
	  return 0;
	case 'd':
	  nis_dom = optarg;
	  break;
	case 'm':
	  map = optarg;
	  break;
	case 'k':
	  key = optarg;
	  break;
	default:
	  print_usage(stderr);
	  return 0;
	}
    }

  if (optind < argc)
    bad_usage("too many arguments");

  if (nis_dom == NULL)
    bad_usage("the -d option is mandatory");

  if (hostname == NULL)
    bad_usage("the -H option is mandatory");

  exit(perform_match(hostname, nis_dom, map, key, timeout, critical, warning));
}
