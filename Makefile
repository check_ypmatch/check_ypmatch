CFLAGS = -O -Wall
CC = gcc
PLUGINDIR = /usr/lib/nagios/plugins
NAGIOSGRAPHERDIR=/etc/nagiosgrapher/ngraph.d/extra/

all: check_ypmatch

check_ypmatch: check_ypmatch.o Makefile
	$(CC) -o $@ check_ypmatch.o -lnsl

clean:
	$(RM) check_ypmatch check_ypmatch.o

install: check_ypmatch
	mkdir -p $(PLUGINDIR)
	cp check_ypmatch $(PLUGINDIR)/check_ypmatch
	if [ -d $(NAGIOSGRAPHERDIR) ]; then \
	    cp check_ypmatch.ncfg $(NAGIOSGRAPHERDIR)/check_ypmatch.ncfg; \
	fi
